def bubble_sort(numbers):
    def swap(i, j):
        numbers[i], numbers[j] = numbers[j], numbers[i]
        
    n = len(numbers)
    swapped = True
    
    x = -1
    
    while swapped:
        swapped = False
        x = x + 1
        for i in range(1, n-x):
            if numbers[i -1] > numbers[i]:
                swap(i -1, i)
                swapped = True
                
    return numbers

if __name__ == "__main__":
    numbers = list(map(int, input("Enter integer number with space: ").split()))
    sorted_numbers = bubble_sort(numbers)
    print("Sorted number is", sorted_numbers)