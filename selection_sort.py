def selection_sort(numbers):
    for i in range(len(numbers)):
        minimum = i
        
        for j in range(i + 1, len(numbers)):
            if numbers[j] < numbers[minimum]:
                minimum = j
                
        numbers[minimum], numbers[i], numbers[minimum]
    return numbers

if __name__ == "__main__":
    numbers = list(map(int, input("Enter integer number with space: ").split()))
    sorted_numbers = selection_sort(numbers)
    print("Sorted number is", sorted_numbers)