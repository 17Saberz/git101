def insertion_sort(numbers, simulation=False):
    for i in range(len(numbers)):
        cursor = numbers[i]
        pos = i
        
        while pos > 0 and numbers[pos - 1] > cursor:
            
            numbers[pos] = numbers[pos-1]
            pos = pos-1
            
        numbers[pos] = cursor
        
    return numbers

if __name__ == "__main__":
    numbers = list(map(int, input("Enter integer number with space: ").split()))
    sorted_numbers = insertion_sort(numbers)
    print("Sorted number is", sorted_numbers)